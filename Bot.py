import socket
import time
import datetime
import re
from BotMethods import *
import random


globalUsers = ["moomasterq","robwiztv"]
modsFile = open("mods.txt","r")#Can move this into BotMethods in case we want mod list updated every hour
mods = modsFile.read()
modsFile.close()

today = str(datetime.date.today())
irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
irc.connect( (getServer() , getPort()) )
irc.send("CAP REQ :twitch.tv/membership \n")
irc.send("CAP REQ" + " :twitch.tv/commands \n")
irc.send("PASS " + getBotPass() + "\n")
irc.send("NICK "+ getBotNick() +"\n")
irc.send("USER "+ getBotNick() + "\n")
irc.send("JOIN " + getChannel() + "\n")
irc.send("PRIVMSG " + getChannel() + " :.mods"+ "\r\n")


def ping():
	irc.send("PONG :pingis\n")

def msg(message):
	irc.send("PRIVMSG " + getChannel() + " :" + message + "\r\n")

if random.random() < 0.1:#Startup message every 10 hours in perfect world
    msg("Beep boop, LesserMooBot has started")#Friendly startup message


while True:
    data = irc.recv(2048)
    user = data.split('!')[0]
    user = re.sub("[^a-zA-Z0-9_]","", user)#Cleans the non-name characters from the string
    log = open("today/log.txt","a")
    log.write(clean(data, user))
    log.close()
    print data
    if data.find("PING :") != -1: 
    	ping()
    if data.find(":!history ") != -1 and ((user in mods) or (user in globalUsers)):
        u = data.split(":!history ")[1]
        if not u.count(' ') != 0:
            history(u.lower(),user.lower())
            msg("bot.whale-net.net/lessermoobot/history/" + u.rstrip("\n").lower() + ".txt")
        else:
            date = u.split(" ")[1].rstrip("\r\n")
            if os.path.exists(date[:7]) and os.path.exists(date[:7] + "/" + date + "/log.txt"):
                u = u.split(" ")[0]
                historyDate(u.lower(),user.lower(),date)
                msg("bot.whale-net.net/lessermoobot/history/" + u.rstrip("\n").lower() + ".txt")
    if data.find(":!convolog ") != -1 and user in ((user in mods) or (user in globalUsers)):
        u = data.split(":!convolog ")[1].rstrip("\r\n")
        if u.count(' ') != 0:
            u2 = u.split(" ")[1]
            u = u.split(" ")[0]
            convolog(u.lower(), u2.lower(), user.lower())
            msg("bot.whale-net.net/lessermoobot/history/" + u.lower() + "-" + u2.lower() + ".txt")
    if (data.find(":!highlight") != -1) or (data.find(":highlight") != -1):
        log = open("today/log.txt","a")
        log.write(highlight(user))
        log.close()
    if user == "twitchnotify" and isSubMessage(data):
    	if isReSub(data) == False:
			msg("/me robwizMustard robwizPeace robwizMustard Thank you for subscribing " + subName(data) + "! robwizMustard robwizPeace robwizMustard" )
	if user == "twitchnotify" and isSubMessage(data):
		if isReSub(data):
			msg("/me robwizDance robwizMustard robwizPeace robwizMustard robwizDance Thank you for subscribing " + subName(data) + " ! " + subMonth(data) +" months, wow! robwizDance robwizMustard robwizPeace robwizMustard robwizDance" )
    if data.find(":!thinice") != -1 and ((user in mods) or (user in globalUsers)):
        u = data.split(":!thinice")[1]
        if not u.count(' ') != 0:
            msg(getThinIce())
        else:
            icer = u.split(" ")[1].rstrip("\r\n ")
            if icer == "clearIce":
                thinClear()
            else:
                msg(thinIce(icer))
    if data.find(":!nextgame") != -1 and ((user in mods) or (user in globalUsers)):
        u = data.split(":!nextgame")[1]
        if not u.count(' ') != 0:
            msg(getNextGame())
        else:
            nextUser = u.split(" ")[1].rstrip("\r\n ")
            if nextUser == "clearNextGame":
                clearNextGame()
            else:
                msg(nextGame(nextUser))
