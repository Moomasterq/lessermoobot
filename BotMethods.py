import datetime
import re
import os
import shutil


server = "irc.twitch.tv"
channel = "#robwiztv"
botnick = "LesserMooBot"
botpass = ""
port = 6667

def getServer():
	return server
def getChannel():
	return channel
def getBotNick():
	return botnick
def getBotPass():
	return botpass
def getPort():
	return port

def clean(data, user):
	m = ""
	currentTime = str(datetime.datetime.now().time())[0:5]
	if not data[0:6] == "PING :":
		if not data[1:6] == "jtv M":
			mType = data.split(".tv ")[1]
			mType = mType.split(" #")[0]
			if mType == "NOTICE":
				modsTemp = data.split(":")[2]
				if (modsTemp[:5] == "This ") or (modsTemp[:4] == "Now ") or (modsTemp[:7] == "Exited "):
					return ""
				else:
					mods = data.split(": ")[1]
					mods = mods.split(", ")
					mods[len(mods)-1] = mods[len(mods)-1].strip('\r\n')
					modsFile = open("mods.txt","w")
					modsFile.write(str(mods))
					modsFile.close()
					return ""
			elif mType == "PRIVMSG":
				message = data[1:].split(":",1)[1:]
				for a in message:
					m = m + a.strip('\n')
				if not user == "":
				    return currentTime + " <" + user + "> " + m + "\n"
				else:
					return ""
			elif mType == "CLEARCHAT":
				b = ""
				ban = data[1:].split(" :")[1:]
				for a in ban:
					b = b + a.strip('\n\r')
				return currentTime + " <" + b + "> " + "CLEARCHAT for " + b + "@" + currentTime + "\n"
			else:
				return ""
		else:
			return ""
	else:
		return ""

def history(historyUser, commandUser):
	hu = re.sub("[^a-zA-Z0-9_]","", historyUser)
	cu = re.sub("[^a-zA-Z0-9_]","", commandUser)
	if os.path.isfile("history/" + hu + ".txt"):
		os.remove("history/" + hu + ".txt")
	txt = open("history/" + hu + ".txt","w")
	with open("today/log.txt","r") as f:
		l = f.readlines()
	for str in l:
		if hu == str[7:7+len(hu)]:
			txt.write(str)
def historyDate(historyUser, commandUser, date):
	hu = re.sub("[^a-zA-Z0-9_]","", historyUser)
	cu = re.sub("[^a-zA-Z0-9_-]","", commandUser)
	if os.path.isfile("history/" + hu + ".txt"):
		os.remove("history/" + hu + ".txt")
	txt = open("history/" + hu + ".txt","w")
	with open(date[:7] + "/" + date + "/log.txt","r") as f:
		l = f.readlines()
	for str in l:
		if hu == str[7:7+len(hu)]:
			txt.write(str)			
def subName(data):
	sub = data.split(channel + " :")[1]
	sub = sub.split(" ")[0]
	return sub
def subMonth(data):
	sub = subName(data)
	month = data.split(sub)[1]
	month = month.split("for ")[1]
	month = month.split("months")[0]
	return month
def isReSub(data):
	sub = subName(data)
	reSub = data.split(sub + " ")[1]
	reSub = reSub.split(" ")[0]
	if reSub == "just":
		return False
	else:
		return True
def isSubMessage(data):
	sub = data.split(channel + " :")[1]
	msg = data.split(" ")
	if "resubscribed" in msg:
		return False
	else:
		return True
def highlight(user):
	currentTime = str(datetime.datetime.now().time())[0:5]
	return currentTime + " <highlight_stream> by " + user + "\n"
def convolog(historyUser, secondUser, commandUser):
	hu = re.sub("[^a-zA-Z0-9_]","", historyUser)
	su = re.sub("[^a-zA-Z0-9_]","", secondUser)
	cu = re.sub("[^a-zA-Z0-9_]","", commandUser)
	if os.path.isfile("history/" + hu + "-" + su + ".txt"):
		os.remove("history/" + hu + "-" + su + ".txt")
	txt = open("history/" + hu + "-" + su + ".txt","w")
	with open("today/log.txt","r") as f:
		l = f.readlines()
	for str in l:
		if hu == str[7:7+len(hu)] or su == str[7:7+len(hu)]:
			txt.write(str)
def thinIce(icer):
	iceText = open("thinice.txt","r")
	out = iceText.read()
	iceText.close()
	iceText = open("thinice.txt","a")
	iceText.write(icer + ", ")
	iceText.close()
	return out[:len(out)-1] + " " + icer
def getThinIce():
	iceText = open("thinice.txt","r")
	out = iceText.read()
	iceText.close()
	return out[:len(out)-2]
def thinClear():
	iceText = open("thinice.txt","w")
	iceText.write("First and Last List: ")
	iceText.close()
def nextGame(nextUser):
	gameText = open("nextgame.txt","r")
	out = gameText.read()
	gameText.close()
	gameText = open("nextgame.txt","a")
	gameText.write(nextUser + ", ")
	gameText.close()
	return out[:len(out)-1] + " " + nextUser
def getNextGame():
	gameText = open("nextgame.txt","r")
	out = gameText.read()
	gameText.close()
	return out[:len(out)-2]
def clearNextGame():
	gameText = open("nextgame.txt","w")
	gameText.write("Users for next game: ")
	gameText.close()